pipeline {
    agent {
        kubernetes {
            label "jenkins-worker-${UUID.randomUUID().toString()}"
            cloud 'kubernetes-devops'
            yaml """
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: ui-user
spec:
  containers:

    - name: docker
      image: docker
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock-volume
    - name: kubectl
      image: lachlanevenson/k8s-kubectl:v1.10.5
      command:
      - cat
      tty: true
  volumes:
  - name: docker-sock-volume
    hostPath:
      path: /var/run/docker.sock
      type: File
"""
        }
    }
    stages {
        stage ('Checkout Git-Repo') {
            steps {
                script {
                    def gitRepo = checkout scm
                    def gitCommit = gitRepo.GIT_COMMIT
                    def appName = "workflows-server"
                    def registryHost = "registry.eparkstation.de"
                    def registryPath = "/moare/"

                    env.APP_NAME = appName
                    env.REGISTRY_HOST = registryHost
                    env.IMAGE_NAME = "${registryHost}${registryPath}${appName}"
                    env.TAG_DEV = "${gitCommit[0..6]}"
                }
            }
        }

        stage ("Create and upload Docker-Image for Development-System") {
            when {
                branch 'dev'
            }
            steps {
                container('docker') {
                    withCredentials([[
                        $class: 'UsernamePasswordMultiBinding',
                        credentialsId: '6f1441dc-f27c-4836-8cc6-94410fb9fbee',
                        usernameVariable: 'USERNAME',
                        passwordVariable: 'PASSWORD']])
                    {
                        sh """
                            docker login -u $USERNAME -p $PASSWORD ${REGISTRY_HOST}
                            docker build -t ${IMAGE_NAME}:${TAG_DEV} .
                            docker push ${IMAGE_NAME}:${TAG_DEV}
                        """
                    }
                }
            }
        }

        stage('Deploy on Development-System') {
            when {
                branch 'dev'
            }
            steps {
                container('kubectl') {
                    withCredentials([
                        file(credentialsId: '8915f5be-f996-4825-aaac-649fc1a54f7a', variable: 'KUBECONFIG')
                    ]) {
                        sh """
                            sed 's#${IMAGE_NAME}:latest#'${IMAGE_NAME}:${TAG_DEV}'#' kube/dev/workflows-server_deploy.yaml | kubectl apply --kubeconfig $KUBECONFIG -f - && \
                            kubectl rollout status deployment/workflows-server --namespace=camunda --kubeconfig $KUBECONFIG
                        """
                    }
                }
            }
        }
    }

    post {

        failure {
            emailext body: '$DEFAULT_CONTENT',
                recipientProviders: [
                    [$class: 'CulpritsRecipientProvider'],
                    [$class: 'DevelopersRecipientProvider']
                ],
                replyTo: '$DEFAULT_REPLYTO',
                subject: '$DEFAULT_SUBJECT',
                to: '$DEFAULT_RECIPIENTS'

        }

    }
}

