# build stage
FROM maven:3.5.4-jdk-11 as build-stage
WORKDIR /app
COPY . /app
RUN mvn install -f /app/plugins/notify-mail/pom.xml

# production stage
FROM camunda/camunda-bpm-platform:7.10.0 as production-stage

COPY --from=build-stage /app/plugins/notify-mail/lib/ /camunda/lib/
COPY --from=build-stage /app/conf/bpm-platform.xml /camunda/conf/
COPY --from=build-stage /app/conf/engine-rest-web.xml /camunda/webapps/engine-rest/WEB-INF/web.xml
COPY --from=build-stage /app/conf/index.jsp /camunda/webapps/ROOT/

EXPOSE 8080 8000

WORKDIR /camunda
USER camunda

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["./camunda.sh"]
