package org.camunda.bpm.tasklistener;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.context.Context;

/**
 * Task listener to be executed when a user task is entered
 */
public class InformAssigneeTaskListener implements TaskListener {

  private final Logger LOGGER = Logger.getLogger(this.getClass().getName());
  public static List<String> assigneeList = new ArrayList<String>();

  private static InformAssigneeTaskListener instance = null;

  protected InformAssigneeTaskListener() { }

  public static InformAssigneeTaskListener getInstance() {
    if(instance == null) {
      instance = new InformAssigneeTaskListener();
    }
    return instance;
  }

  public void notify(DelegateTask delegateTask) {
    String taskId = delegateTask.getId();
    String assignee = delegateTask.getAssignee();
    assigneeList.add(assignee);

    if (assignee != null) {
      
      // Get User Profile from User Management
      IdentityService identityService = Context.getProcessEngineConfiguration().getIdentityService();
      User user = identityService.createUserQuery().userId(assignee).singleResult();

      if (user != null) {
        
        // Get Email Address from User Profile
        String recipient = user.getEmail();
        
        if (recipient != null && !recipient.isEmpty()) {
          Properties props = new Properties();
          props.put("mail.smtp.host", System.getenv("MAIL_HOST"));
          props.put("mail.smtp.socketFactory.port", System.getenv("MAIL_PORT"));
          props.put("mail.smtp.socketFactory.class",
              "javax.net.ssl.SSLSocketFactory");
          props.put("mail.smtp.auth", "true");
          props.put("mail.smtp.port", System.getenv("MAIL_PORT"));

          Session session = Session.getDefaultInstance(props,
            new javax.mail.Authenticator() {
              protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(System.getenv("MAIL_USERNAME"),System.getenv("MAIL_PASSWORD"));
              }
            });

          try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("noreply@tonifreitag.de"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(recipient));
            message.setSubject("[WORKFLOWS] New Task: " + delegateTask.getName());
            message.setText("There is a new task for you: " + System.getenv("HOST_WORKFLOWS") + "/tasks/" + taskId);

            Transport.send(message);

            LOGGER.info("Task Assignment Email successfully sent to user '" + assignee + "' with address '" + recipient + "'.");

          } catch (MessagingException e) {
            LOGGER.log(Level.WARNING, "Could not send email to assignee", e);
            throw new RuntimeException(e);
          }

        } else {
          LOGGER.warning("Not sending email to user " + assignee + "', user has no email address.");
        }

      } else {
        LOGGER.warning("Not sending email to user " + assignee + "', user is not enrolled with identity service.");
      }

    }
  }

}