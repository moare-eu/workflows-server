package org.camunda.bpm.tasklistener.plugin;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.camunda.bpm.engine.impl.bpmn.parser.BpmnParseListener;
import org.camunda.bpm.engine.impl.cfg.AbstractProcessEnginePlugin;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;

import java.util.ArrayList;
import java.util.List;

public class TaskListenerPlugin extends AbstractProcessEnginePlugin {

    protected Logger LOG = Logger.getLogger(TaskListenerPlugin.class.getName());

    @Override
    public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
        LOG.log(Level.INFO, "PLUGIN {0} activated on process engine {1}", new String[]{getClass().getSimpleName(), processEngineConfiguration.getProcessEngineName()});

        List<BpmnParseListener> preParseListeners = processEngineConfiguration.getCustomPreBPMNParseListeners();
        if(preParseListeners == null) {
            preParseListeners = new ArrayList<BpmnParseListener>();
            processEngineConfiguration.setCustomPreBPMNParseListeners(preParseListeners);
        }
        preParseListeners.add(new TaskListener());
    }
}